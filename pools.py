from flask import Flask, request, render_template
import xml.etree.ElementTree as ET
import requests

app = Flask(__name__)


@app.route('/')
def index():
    wcl = request.args.get('weekday_closure')
    plt = request.args.get('pool_type')
    wkd = request.args.get('weekend')
    wcl_list, plt_list, wkd_list = [], [], []
    wcl_plt, wcl_wkd, plt_wkd, wcl_plt_wkd = [], [], [], []
    for i in range(len(l4)):
        if wcl == l4[i] and plt is None and wkd is None:
            wcl_list.append(l1[i])
            continue
        elif plt == l2[i] and wkd is None and plt is None:
            plt_list.append(l1[i])
            continue
        elif wkd == l3[i] and plt is None and wcl is None:
            wkd_list.append(l1[i])
            continue
        elif wcl == l4[i] and plt == l2[i] and wkd is None:
            wcl_plt.append(l1[i])
            continue
        elif wcl == l4[i] and wkd == l3[i] and plt is None:
            wcl_wkd.append(l1[i])
            continue
        elif wkd == l3[i] and plt == l2[i] and wcl is None:
            plt_wkd.append(l1[i])
            continue
        elif wkd == l3[i] and plt == l2[i] and wcl == l4[i]:
            wcl_plt_wkd.append(l1[i])
            continue

    if wcl is not None and plt is None and wkd is None:
        return render_template("Final_list.html", len=len(wcl_list), n1=wcl_list)
    elif wcl is None and plt is not None and wkd is None:
        return render_template("Final_list.html", len=len(plt_list), n1=plt_list)
    elif wcl is None and plt is None and wkd is not None:
        return render_template("Final_list.html", len=len(wkd_list), n1=wkd_list)
    elif wcl is not None and plt is not None and wkd is None:
        return render_template("Final_list.html", len=len(wcl_plt), n1=wcl_plt)
    elif wcl is not None and plt is None and wkd is not None:
        return render_template("Final_list.html", len=len(wcl_wkd), n1=wcl_wkd)
    elif wcl is None and plt is not None and wkd is not None:
        return render_template("Final_list.html", len=len(plt_wkd), n1=plt_wkd)
    elif wcl is not None and plt is not None and wkd is not None:
        return render_template("Final_list.html", len=len(wcl_plt_wkd), n1=wcl_plt_wkd)
    elif wcl is None and plt is None and wkd is None:
        return "Welcome to Austin Pool Information Website."


src = "https://raw.githubusercontent.com/devdattakulkarni/elements-of-web-programming/master/data/austin-pool-timings.xml"
data = requests.get(src)
root = ET.fromstring(data.text)
l1, l2, l3, l4 = [], [], [], []
for pool in root.findall('row'):
    pool_name = ''
    weekend = ''
    pool_type = ''
    weekday_closure = ''
    try:
        pool_name = pool.find('pool_name').text
        weekend = pool.find('weekend').text
        pool_type = pool.find('pool_type').text
        weekday_closure = pool.find('weekday_closure').text
        l1.append(pool_name)
        l2.append(pool_type)
        l3.append(weekend)
        l4.append(weekday_closure)
    except AttributeError:
        continue
if __name__ == '__main__':
    app.run()
